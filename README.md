<div align="center">
  <img alt="JavaScript" src="https://img.shields.io/badge/javascript%20-%23323330.svg?&style=for-the-badge&logo=javascript&logoColor=white"/>
  <img alt="HTML5" src="https://img.shields.io/badge/html5%20-%23323330.svg?&style=for-the-badge&logo=html5&logoColor=white"/>
  <img alt="CSS3" src="https://img.shields.io/badge/css3%20-%23323330.svg?&style=for-the-badge&logo=css3&logoColor=white"/>
</div>

# Cursed Snake

Cursed Snake is a simple Snake game built using HTML, JavaScript, and CSS. This project hosts the files for the game, and you can play it online by visiting [Cursed Snake](https://cursedprograms.github.io/cursed-snake/).

## Demo

Check out the live demo: [Cursed Snake Demo](https://cursedprograms.github.io/cursed-snake/)

## Features

- **Classic Snake Gameplay:** Navigate the snake to eat food and grow longer.
- **Score Tracking:** Keep track of your score as you eat more food.
- **Responsive Design:** Play the game on different devices.

## How to Play

1. Visit the [Cursed Snake Demo](https://cursedprograms.github.io/cursed-snake/).
2. Use arrow keys to control the snake's direction.
3. Eat the food to grow longer.
4. Avoid colliding with the snake's own body and the edges of the screen.

## Technologies Used

- HTML
- JavaScript
- CSS

## Contributing

If you're interested in contributing to Cursed Snake, feel free to fork the repository and submit a pull request. Contributions are always welcome!

## License

This project is licensed under the [MIT License](LICENSE).

---

Enjoy playing Snake!
